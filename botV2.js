// import puppeteer from 'puppeteer';
import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import AdblockerPlugin from 'puppeteer-extra-plugin-adblocker';

puppeteer.use(StealthPlugin());
puppeteer.use(AdblockerPlugin({ blockTrackers: true }))

import configs from './configs.js';

var hasCrateTime = false;

// const args = process.argv.slice(2);

const blockedDomains = [
  'googlesyndication.com',
  'adservice.google.com',
  'www.google.com',
  'stats.g.doubleclick.net',
  'www.google-analytics.com',
  'google-analytics.com',
  'www.googletagmanager.com',
  'googletagmanager.com',
  '.jpg',
  '.jpeg',
  '.png',
  '.gif',
  // '.css',
];

function getArgs() {
  const args = {};
  process.argv
    .slice(2, process.argv.length)
    .forEach(arg => {
      // long arg
      if (arg.slice(0, 2) === '--') {
        const longArg = arg.split('=');
        const longArgFlag = longArg[0].slice(2, longArg[0].length);
        const longArgValue = longArg.length > 1 ? longArg[1] : true;
        args[longArgFlag] = longArgValue;
      }
      // flags
      else if (arg[0] === '-') {
        const flags = arg.slice(1, arg.length).split('');
        flags.forEach(flag => {
          args[flag] = true;
        });
      }
    });
  return args;
}

const args = getArgs();

const productId = args?.id || configs.productId;
const username = args?.user ?? configs.account.username;
const password = args?.pass ?? configs.account.password;
const url = args?.url ?? configs.productUrl;
const headless = true;

// const dataDir = `./data/${username}`;
// if (!fs.existsSync(dataDir)){
//     fs.mkdirSync(dataDir, { recursive: true });
// }

const minimal_args = [
  '--autoplay-policy=user-gesture-required',
  '--disable-background-networking',
  '--disable-background-timer-throttling',
  '--disable-backgrounding-occluded-windows',
  '--disable-breakpad',
  '--disable-client-side-phishing-detection',
  '--disable-component-update',
  '--disable-default-apps',
  '--disable-dev-shm-usage',
  '--disable-domain-reliability',
  '--disable-extensions',
  '--disable-features=AudioServiceOutOfProcess',
  '--disable-hang-monitor',
  '--disable-ipc-flooding-protection',
  '--disable-notifications',
  '--disable-offer-store-unmasked-wallet-cards',
  '--disable-popup-blocking',
  '--disable-print-preview',
  '--disable-prompt-on-repost',
  '--disable-renderer-backgrounding',
  '--disable-setuid-sandbox',
  '--disable-speech-api',
  '--disable-sync',
  '--hide-scrollbars',
  '--ignore-gpu-blacklist',
  '--metrics-recording-only',
  '--mute-audio',
  '--no-default-browser-check',
  '--no-first-run',
  '--no-pings',
  '--no-sandbox',
  '--no-zygote',
  '--password-store=basic',
  '--use-gl=swiftshader',
  '--use-mock-keychain',
  '--disable-gl-drawing-for-tests',
];

const delay = (time) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, time)
  });
}

const goLogin = async (page) => {
  console.log('--------------------------------------------')
  console.log('Go to login page.....');
  console.log('--------------------------------------------')

  try {
    // กด SingIn เข้าสู่ระบบ
    const signIn = '#lbMember > a';
    await page.waitForSelector(signIn);
    await page.evaluate(() => {
      document.querySelector('#lbMember > a').click();
    });
    await page.waitForNavigation({ timeout: 20000, waitUntil: 'domcontentloaded' });

    // Pahe Login Form
    const loginSelector = '#login-with-password-button';
    await page.waitForSelector(loginSelector);
    await page.click(loginSelector);

    const passwordFormSelector = '#password_form';
    await page.waitForSelector(passwordFormSelector);
    await delay(1000);

    console.log('--------------------------------------------')
    console.log('🔹 username ========>', username);
    console.log('🔹 password ========>', password);
    console.log('--------------------------------------------')

    await page.type('input[name=username]', username);
    await page.type('input[name=password]', password);

    await page.$eval('input[type=submit]', (el) => el.click());

    try {
      await page.waitForNavigation({ waitUntil: 'domcontentloaded' })
    } catch (error) {
      console.log('login is timeout')
    }
    await delay(1000);


    console.log('--------------------------------------------')
    console.log('✅ Login is successful.....');
    console.log('--------------------------------------------')
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('❌ Login is failed.....');
    console.log('--------------------------------------------')
  }
}

const addToCart = async (page, browser) => {
  try {
    const addCartAreaSelector = '.addCartArea';
    const buyNowSelector = '.buy_now';
    await page.waitForSelector(addCartAreaSelector, { timeout: 5000 });
    await page.click(buyNowSelector);
    await page.waitForNavigation({ timeout: 10000, waitUntil: 'domcontentloaded' })

    console.log('--------------------------------------------')
    console.log('Step1. ✅ Add to cart is successful.....');
    console.log('--------------------------------------------')

    return true;
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('Step1. ❌ Add to cart is failed.....');
    console.log('--------------------------------------------')
    return false;
  }
}

const buyAndCheckOut = async (page, browser) => {
  try {
    //  const checkoutNow = 'button[type="button"].primary.v-btn.v-size--x-large';
    //   await page.waitForSelector(checkoutNow, { timeout: 10000 });
    await page.evaluate(() => {
      document.querySelectorAll('button[type="button"].primary.v-btn.v-size--x-large')[0].click()
    })

    console.log('--------------------------------------------')
    console.log('Step2. ✅ Check out is successful.....');
    console.log('--------------------------------------------')
    return true;
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('Step2. ❌ Check out is failed.....');
    console.log('--------------------------------------------')
    return false;
  }
}

const setAddress = async (page, browser) => {
  try {
    // const addressAreaSelector = 'input[type="radio"]';
    // await page.waitForSelector(addressAreaSelector, { timeout: 10000 });
    await page.evaluate(() => {
      document.querySelectorAll('input[type="radio"]')[0].click()
    });

    console.log('--------------------------------------------')
    console.log('Step3. ✅ Select Address is successful.....');
    console.log('--------------------------------------------')
    return true;
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('Step3. ❌ Select Address is failed.....');
    console.log('--------------------------------------------')
    return false;
  }
}

const checkoutWithAddress = async (page, browser) => {
  try {
    // const checkoutNow = 'button[type="button"].primary.v-btn.v-size--x-large';
    // await page.waitForSelector(checkoutNow, { timeout: 10000 });
    await page.evaluate(() => {
      document.querySelectorAll('button[type="button"].primary.v-btn.v-size--x-large')[0].click()
    });

    console.log('--------------------------------------------')
    console.log('Step4. ✅ Check out with address is successful.....');
    console.log('--------------------------------------------')
    return true;
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('Step4. ❌ Check out with address is failed.....');
    console.log('--------------------------------------------')
    return false;
  }
}

const selectShipment = async (page, browser) => {
  try {
    await page.evaluate(() => {
      document.querySelectorAll('.v-stepper__content')[1].querySelectorAll('input[type="radio"]')[0].click()
    });

    console.log('--------------------------------------------')
    console.log('Step5. ✅ Select shipment is successful.....');
    console.log('--------------------------------------------')
    return true;
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('Step5. 🟡 Select shipment is not success.....');
    console.log('--------------------------------------------')
  }
}

const goNextToPayment = async (page, browser) => {
  try {
    await page.evaluate(() => {
      document.querySelectorAll('button[type="button"].primary.v-btn.v-size--x-large')[1].click()
    })

    console.log('--------------------------------------------')
    console.log('Step6. ✅ Next to payment is successful.....');
    console.log('--------------------------------------------')
    return true;
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('Step6. 🟡 Next to payment is not click.....');
    console.log('--------------------------------------------')
  }
}

const successStep = async (page, browser) => {
  try {
    await page.evaluate(() => {
      document.querySelectorAll('button[type="button"].primary.v-btn.v-size--x-large')[0].click()
    })
    await page.waitForNavigation({ timeout: 20000, waitUntil: 'domcontentloaded' })

    console.log('--------------------------------------------')
    console.log('Step7. ✅ Successful.....');
    console.log('--------------------------------------------')

    return true;
  } catch (error) {
    console.log('--------------------------------------------')
    console.log('Step7. ❌ Failed.....');
    console.log('--------------------------------------------')
  }
}

const checkStock = async (page) => {
  try {
    const pageContent = await page.content();
    if (pageContent.includes('ขณะนี้ร้านค้าได้ปิดร้านชั่วคราว หากมีข้อสงสัยต่างๆ กรุณาติดต่อร้านค้าที่ช่องทางต่างๆด้านล่างนี้')) {
      console.log('ขณะนี้ร้านค้าได้ปิดร้านชั่วคราว');
      return false
    } else {
      return true;
    }
  } catch (error) {
    console.log(error)
    return false;
  }
}

const goCheckout = async (page, browser) => {
  if (hasCrateTime === false) {
    console.time();
    console.log('\n Ready to Shopping \n');
    hasCrateTime = true;
  }

  await delay(500)

  try {
    await page.click('.accept_button');
    console.log('--------- Accept Cookie Success ---------')
  } catch (error) {
    console.log('--------- No Accept Button ---------')
  }

  const adToCartResult = await addToCart(page, browser);
  await delay(500)
  try {
    while (true) {
      await delay(1000);
      const hasStock = await checkStock(page);
      console.log('--------------------------------------------')
      console.log(`Stock.....`, hasStock);
      console.log('--------------------------------------------')

      if (hasStock) {
        break;
      } else {
        await delay(2000);
        await page.reload();
        continue;
      }
    }
  } catch (error) {
    console.log('error', error)
    await page.reload();
  }

  const buyAndCheckOutResult = await buyAndCheckOut(page, browser);

  await delay(500)

  const setAddressResult = await setAddress(page, browser);

  await delay(500)

  const checkoutWithAddressResult = await checkoutWithAddress(page, browser);

  await delay(500)

  const selectShipmentResult = await selectShipment(page, browser);

  await delay(500)

  const nextToPaymentResult = await goNextToPayment(page, browser);

  await delay(500)

  const successStepResult = await successStep(page, browser);

  const isAllSuccess = [adToCartResult, buyAndCheckOutResult, setAddressResult, checkoutWithAddressResult, successStepResult].every((isSuccess) => isSuccess === true);
  if (isAllSuccess) {
    console.timeEnd();
    await browser.close();
  } else {
    goRun(true);
  }

  // const isAllSuccess = [adToCartResult, buyAndCheckOutResult, setAddressResult, checkoutWithAddressResult, selectShipmentResult, nextToPaymentResult, successStepResult].every((isSuccess) => isSuccess === true);
  // if (isAllSuccess) {
  //   await browser.close();
  // } else {
  //   goRun(true);
  // }
}

const clearCart = async (page) => {
  try {
    await page.waitForSelector('.new-u-cart .actionPopupButton');

    await page.evaluate(() => {
      document.querySelector('.new-u-cart .actionPopupButton').click();
    });

    await page.waitForNavigation()

    await page.evaluate(() => {
      document.querySelectorAll('.remove').forEach((el) => {
        el.click()
      })
    });

    await delay(1000)

    await page.evaluate(() => {
      document.querySelectorAll('.button-true').forEach((el) => {
        el.click()
      })
    });

    await delay(1000)

    await page.goBack();
  } catch (error) {
    await page.goBack();
    console.log(error)
  }
}

const getProductDetail = async (page) => {
  try {
    // let productNameElement = await page.$('.productHeaderNew > .headerText')
    let productCodeElement = await page.$('.product_code')

    let product = await page.evaluate((code) => ({ code: code.textContent }), productCodeElement)

    // console.log('Product Name: =====>', product.name)
    console.log('Product Code: =====>', product?.code)
    console.log('--------------------------------------------')
    return true;
  } catch (error) {
    console.log('Product Name: =====>', "Can't get product name")
    console.log('Product Code: =====>', "Can't get product code")
    console.log('--------------------------------------------')
    return false;
  }
}

let parentPage, parentBrowser;
let retryCount = 0;
let retryMax = 5;

const goRun = async (isLoggedIn = false) => {
  if (isLoggedIn === true) {
    if (retryCount >= retryMax) {
      await parentBrowser.close();
      return;
    }

    retryCount = retryCount + 1;
    console.log(`\n Retry (${retryCount}/${retryMax}).... \n`);
  } else {
    parentBrowser = await puppeteer.launch({
      headless: true,
      args: minimal_args,
      // userDataDir: dataDir
    });

    parentPage = await parentBrowser.newPage();

    await parentPage.setRequestInterception(true);

    parentPage.on('request', (req) => {
      const url = req.url();
      const resourceType = req.resourceType();
      if ([
        'image',
        // 'stylesheet',
        // 'font',
        // 'media',
        'favicon',
        'texttrack',
        'object',
        'beacon',
        'csp_report',
        'imageset',
      ].indexOf(resourceType) !== -1) {
        req.abort();
      } else if (blockedDomains.some(domain => url.includes(domain))) {
        req.abort();
      } else {
        req.continue();
      }
    });

    await parentPage.setViewport({
      width: 1280,
      height: 960,
      deviceScaleFactor: 1,
    });
  }

  const productUrl = `${url}/product/${productId}`;

  console.log('--------------------------------------------')
  console.log('Product Id: =====>', productId);
  console.log('Product Url: =====>', productUrl)

  // loop for check product status
  let count = 0;

  while (true) {
    try {
      await parentPage.goto(productUrl, { waitUntil: 'domcontentloaded' });

      if (count === 0 && isLoggedIn === false) {
        await goLogin(parentPage);
      }

      await parentPage.goto(productUrl, { waitUntil: 'domcontentloaded' });

      const isHasProduct = await getProductDetail(parentPage, parentBrowser)
      console.log('isHasProduct', isHasProduct);

      if (isHasProduct) {
        await delay(100);

        await parentPage.waitForSelector('.productDetail', { timeout: 5000 });

        const hasProduction = await parentPage.$eval('div.buy_now_text', div => div.innerText.trim());

        console.log('--------------------------------------------')
        console.log(`✅ Success: ====>`, hasProduction);
        console.log('--------------------------------------------')

        await goCheckout(parentPage, parentBrowser);

        break;
      } else {
        count = count + 1;
        await delay(2000);
        continue;
      }

    } catch (e) {
      await delay(2000);
      count = count + 1;

      if (e === 'continue') {
        console.log('--------------------------------------------')
        console.error(`🟡 Re try (${count})`);
        console.log('--------------------------------------------')
      } else {
        console.log('--------------------------------------------')
        console.error(`🟡 Warning (${count})`);
        console.log('--------------------------------------------')
      }

      continue;
    }
  }
}

goRun();