import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import AdblockerPlugin from 'puppeteer-extra-plugin-adblocker';

puppeteer.use(StealthPlugin());
puppeteer.use(AdblockerPlugin({ blockTrackers: true }))


const blockedDomains = [
  'googlesyndication.com',
  'adservice.google.com',
  'www.google.com',
  'stats.g.doubleclick.net',
  'www.google-analytics.com',
  'google-analytics.com',
  'www.googletagmanager.com',
  'googletagmanager.com',
  '.jpg',
  '.jpeg',
  '.png',
  '.gif',
  '.css',
];

const minimal_args = [
  '--force-device-scale-factor=4'
];

const delay = (time) => {
   return new Promise(function(resolve) { 
       setTimeout(resolve, time)
   });
}

let parentPage, parentBrowser;

const goRun = async () => {
    parentBrowser = await puppeteer.launch({
      headless: true,
      args: minimal_args,
    });

    parentPage = await parentBrowser.newPage();

    await parentPage.setRequestInterception(true);

    parentPage.on('request', (req) => {
      const url = req.url();
      if (blockedDomains.some(domain => url.includes(domain))) {
        req.abort();
      } else {
        req.continue();
      }
    });

    await parentPage.setViewport({
      width: 200,
      height: 200,
      deviceScaleFactor: 4,
      pageScaleFactor: 4,
    });
  
    while (true) {
      await parentPage.goto('https://www.powerbuy.co.th/th/promotion/pre-order/pre-order-iphone-15/iphone-15-pro-max');
      await parentPage.screenshot({ 'path': `screenshot/iphone15/${Date.now()}.png` })
      await delay(10000);
      continue;
    }
}
  
goRun();


